package Encapsulation;

public class StudentEncapeDemo {

	public static void main(String[] args) {
		
		Student studentRaja= new Student();
		studentRaja.setName("Raja");
		studentRaja.setAddress("Colombo");
		studentRaja.setBatch("CSD-16");
		studentRaja.setAge(20);
		
		Student studentRani= new Student();
		studentRani.setName("Rani");
		studentRani.setAddress("Jaffna");
		studentRani.setBatch("CSD-17");
		studentRani.setAge(18);
		
		System.out.println(studentRaja.getName());
		System.out.println(studentRani.getName()+"-"+studentRani.getAge());
		
		System.out.println(studentRaja);
	}
}
